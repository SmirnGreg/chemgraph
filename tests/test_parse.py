from alchemic_chemgraph.snapshot import Snapshot
import pytest

ENTRY = """
#----------------------------------------------------------------------------------------------------
# D              at  4.71E+00 [years] n_dot= 4.888D-11 n/n(H)= 1.026D-09
#----------------------------------------------------------------------------------------------------
  2.3E+01%  1.2E-11  2718 HD            PHOTON         =>           D             H                                                        3.40E-11  0.00E+00  2.50E+00  4.71E+00
  8.9E+00%  4.6E-12 11897 CH2D+         C              =>           D             C2H2+                                                    6.00E-10  0.00E+00  0.00E+00  4.71E+00
  7.9E+00%  4.1E-12 11374 CD+           oH2            =>           D             CH2+                                                     6.00E-10  0.00E+00  0.00E+00  4.71E+00
  7.0E+00%  3.6E-12 10039 C2D+          oH2            =>           D             C2H2+                                                    5.50E-10  0.00E+00  0.00E+00  4.71E+00
  4.5E+00%  2.3E-12 10615 C2H3D+        C              =>           D             C3H3+                                                    5.00E-10  0.00E+00  0.00E+00  4.71E+00
  3.9E+00%  2.1E-12 11657 CHD+          oH2            =>           D             CH3+                                                     6.00E-10  0.00E+00  0.00E+00  4.71E+00
  3.4E+00%  1.8E-12 25224 O             CD             =>           D             CO                                                       6.60E-11  0.00E+00  0.00E+00  4.71E+00
 -2.9E+00% -1.5E-12   472 D             FREEZE         =>           gD                                                                     1.00E+00  0.00E+00  0.00E+00  4.71E+00
  2.8E+00%  1.4E-12 13092 CH4D+         C              =>           D             C2H4+                                                    5.00E-10  0.00E+00  0.00E+00  4.71E+00
  2.6E+00%  1.4E-12 11375 CD+           pH2            =>           D             CH2+                                                     6.00E-10  0.00E+00  0.00E+00  4.71E+00
  2.3E+00%  1.2E-12 10040 C2D+          pH2            =>           D             C2H2+                                                    5.50E-10  0.00E+00  0.00E+00  4.71E+00
  2.2E+00%  1.1E-12 24000 C             C2D            =>           D             C3                                                       1.00E-10  0.00E+00  0.00E+00  4.71E+00
  1.8E+00%  9.3E-13 10136 C2HD+         C              =>           D             C3H+                                                     5.50E-10  0.00E+00  0.00E+00  4.71E+00
  1.7E+00%  8.6E-13 24003 C             C2HD           =>           D             C3H                                                      7.25E-11 -1.20E-01  0.00E+00  4.71E+00
  1.5E+00%  7.6E-13 24024 C             CD             =>           D             C2                                                       6.59E-11  0.00E+00  0.00E+00  4.71E+00
  1.5E+00%  7.6E-13 25052 N             CD             =>           D             CN                                                       1.66E-10 -9.00E-02  0.00E+00  4.71E+00
  1.4E+00%  7.4E-13 35699 O             CHD            =>           D             CO            H                                          1.20E-10  0.00E+00  0.00E+00  4.71E+00
  1.4E+00%  7.3E-13 11002 C3D+          oH2            =>           D             C3H2+                                                    5.00E-13  0.00E+00  0.00E+00  4.71E+00
  1.3E+00%  6.8E-13 11658 CHD+          pH2            =>           D             CH3+                                                     6.00E-10  0.00E+00  0.00E+00  4.71E+00
  1.3E+00%  6.8E-13 26166 DCO+          ELECTR         =>           D             CO                                                       2.80E-07 -6.90E-01  0.00E+00  4.71E+00
  1.0E+00%  5.4E-13 10734 C2H3D+        O              =>           D             CH3CO+                                                   5.00E-11  0.00E+00  0.00E+00  4.71E+00
  8.9E-01%  4.7E-13 10429 C2H2D+        C              =>           D             C3H2+                                                    5.00E-10  0.00E+00  0.00E+00  4.71E+00
  8.0E-01%  4.2E-13 24057 C             OD             =>           D             CO                                                       1.15E-10 -3.39E-01  0.00E+00  4.71E+00
  7.6E-01%  4.0E-13 11129 C3HD+         N              =>           D             C3HN+                                                    1.25E-10  0.00E+00  0.00E+00  4.71E+00
  7.0E-01%  3.6E-13 11896 CHD2+         C              =>           D             C2HD+                                                    6.00E-10  0.00E+00  0.00E+00  4.71E+00
  6.9E-01%  3.6E-13 25015 N             C2D            =>           D             C2N                                                      1.00E-10  0.00E+00  0.00E+00  4.71E+00
  5.8E-01%  3.0E-13 36146 C2H3D+        ELECTR         =>           D             C2H2          H                                          1.85E-07 -7.60E-01  0.00E+00  4.71E+00
  4.7E-01%  2.5E-13   652 gD            DESORB         =>           D                                                                      1.00E+00  8.50E-01  6.24E+02  4.71E+00
  4.7E-01%  2.4E-13 11003 C3D+          pH2            =>           D             C3H2+                                                    5.00E-13  0.00E+00  0.00E+00  4.71E+00
  4.2E-01%  2.2E-13 25232 O             CH2D           =>           D             H2CO                                                     7.00E-11  0.00E+00  0.00E+00  4.71E+00
  3.8E-01%  2.0E-13 10393 C2HD+         O              =>           D             C2HO+                                                    5.00E-11  0.00E+00  0.00E+00  4.71E+00
  """


@pytest.fixture(scope="module")
def parsed_data():
    return Snapshot(ENTRY)


def test_parse_mainspec(parsed_data):
    assert parsed_data.mainspec == "D"


def test_parse_time(parsed_data):
    assert parsed_data.time == 4.71e0


def test_parse_lenreactions(parsed_data):
    assert len(parsed_data.reactions) == 31


def test_parse_lenspecies(parsed_data):
    assert len(parsed_data.species) == 49

@pytest.mark.parametrize(
    'desired_reactants, desired_products',
    (
            ({'gD', 'DESORB'}, {'D', }),
            ({'C', 'CD'}, {'D', 'C2'}),
            ({'C2H3D+', 'ELECTR'}, {'D', 'C2H2', 'H'}),
            pytest.param({'gD', 'DESORB'}, {'D', 'DD'}, marks=pytest.mark.xfail),
    )
)
def test_reaction_is_parsed_properly(desired_reactants, desired_products, parsed_data):
    for reaction in parsed_data.reactions:
        if set(reaction.reactants) == desired_reactants \
                and set(reaction.products) == desired_products:
            return
    assert "Reaction not found!"
