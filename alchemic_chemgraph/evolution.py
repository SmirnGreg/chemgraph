import plotly.graph_objects as go
import alchemic_chemgraph.snapshot

class AlchemicEvolution(object):
    def __init__(self, file, mainspec=None):
        self.snapshots = []
        self.title = None
        with open(file, 'r') as fff:
            started = False
            reading_reaction = 0
            for i, line in enumerate(fff.readlines()):
                if i == 1:
                    self.title = line
                if not started:
                    if 'REACTIONS' in line:
                        started = True
                    continue
                if '#-------' in line:
                    if reading_reaction == 0:
                        reading_reaction = 1
                        entry_lines = []
                    elif reading_reaction == 1:
                        reading_reaction = 2
                    elif reading_reaction == 2:
                        entry = ''.join(entry_lines)
                        snapshot = alchemic_chemgraph.snapshot.Snapshot(entry)
                        if mainspec is None or snapshot.mainspec == mainspec:
                            self.snapshots.append(snapshot)
                        entry_lines = []
                        reading_reaction = 1
                entry_lines.append(line)
        all_species = []
        all_reactions = []
        for snapshot in self.snapshots:
            all_species.extend([spec for spec in snapshot.species])
            all_reactions.extend([reac.reaction_id for reac in snapshot.reactions])
        all_species = list(dict.fromkeys(all_species))
        all_reactions = list(dict.fromkeys(all_reactions))

        for snapshot in self.snapshots:
            snapshot.graph_positions(all_reactions=all_reactions, all_species=all_species)

    def plotly(self):
        """Generate plotly figure"""
        traces = []
        steps = []
        all_traces = []
        trace_begins = [0]
        trace_ends = []
        visible = []
        for snapshot in self.snapshots:
            snapshot_traces = snapshot.plotly_traces()
            traces.extend(snapshot_traces)
            all_traces.append(snapshot_traces)
            trace_ends.append(trace_begins[-1] + len(snapshot_traces))
            trace_begins.append(trace_ends[-1])
            visible.append([snapshot.visible for snapshot in snapshot_traces])

        fig = go.Figure(data=traces,
                        layout=go.Layout(
                            title=self.title,
                            titlefont_size=16,
                            showlegend=True,
                            hovermode='closest',
                            margin=dict(b=20, l=5, r=5, t=40),
                            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False, range=[-1.1, 1.1]),
                            yaxis=dict(showgrid=False, zeroline=False, showticklabels=False, range=[-1.1, 1.1], scaleanchor='x'))
                        )
        for i, snapshot_traces in enumerate(all_traces):
            step = dict(
                method="restyle",
                args=["visible", [False] * len(fig.data)],
                label=self.snapshots[i].time
            )
            step["args"][1][trace_begins[i]:trace_ends[i]] = visible[i]  # Toggle i'th trace to "visible"
            steps.append(step)
        sliders = [dict(
            active=0,
            currentvalue={"prefix": "Time: ", "suffix": " [yr]"},
            pad={"t": 50},
            steps=steps
        )]
        fig.update_layout(
            sliders=sliders
        )
        return fig

if __name__ == '__main__':
    file = '../HCO+_DCO+_model_d.out'
    evo = AlchemicEvolution(file, mainspec='HCO+')
    fig = evo.plotly()
    fig.show()
    # py.plot(fig)