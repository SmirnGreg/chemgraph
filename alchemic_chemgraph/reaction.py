class Reaction(object):
    """Class to parse and store reaction string from ALCHEMIC reaction graph output"""

    def __init__(self, line: str):
        words = line[:120].split()
        self.rdiff_nonabs = float(words[0].rstrip("%")) * 1e-2
        self.rdiff = float(words[0].rstrip("%")) * 1e-2
        self.adiff = float(words[1])
        self.reaction_id = int(words[2])
        self.reactants = []
        self.products = []
        for i in range(3, len(words)):
            if "=>" in words[i]:
                self.products = words[i + 1:]
                break
            else:
                self.reactants.append(words[i])
        self.desc = " + ".join(self.reactants) \
                    + " => " + " + ".join(self.products) \
                    + f" | {self.rdiff * 100:.1f}%"

    def __repr__(self):
        return self.desc