# Installation:

Install development version (to develop locally):

```bash
git clone https://gitlab.com/SmirnGreg/chemgraph.git
cd chemgraph
pip install -e .
```

Normal installation:

```bash
pip install git+https://gitlab.com/SmirnGreg/chemgraph.git
```

# Usage
## Example run

```bash
alchemic_chemgraph HCO+_DCO+_model_d.out
```
Move the slider to select time! Turn reactions on and off by pressing them in legend!

More information on command-line arguments:
```bash
alchemic_chemgraph --help

usage: alchemic_chemgraph [-h] [-o OUTPUTFILENAME] [-s] filename species

ALCHEMIC interactive graph

positional arguments:
  filename              File to process
  species               Species to process

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUTFILENAME, --outputfilename OUTPUTFILENAME
                        Filename (.html) to save the output (optional)
  -s, --show            Show output in browser immediately
```