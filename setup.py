from setuptools import setup

setup(
    name='alchemic-chemgraph',
    version='1.1',
    packages=['alchemic_chemgraph'],
    url='https://gitlab.com/SmirnGreg/chemgraph',
    license='MIT',
    author='Grigorii V. Smirnov-Pinchukov',
    author_email='smirnov@mpia.de',
    description='Visualise graph of ALCHEMIC output',
    scripts=['scripts/alchemic_chemgraph'],
    install_requires=[
        'plotly>=4',
        'numpy',
        'matplotlib>=3',
        'networkx>=2',
        'chart-studio'
    ],
)
